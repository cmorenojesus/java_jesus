

public class Conversor {

    public double mih2kmh(double mih){
        return mih*1.6;
    }

    public double mih2yas(double mih){
        return mih*0.4888;
    }

    public double ms2kmh(double ms){
        return ms*3.6;
    }

    public double ms2mih(double ms){
        return ms*2.23694;
    }

}